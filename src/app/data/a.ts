export const DATA_FOR_CODE_A = [
    {
        name: 'Hitesh Sharma',
        role: 'Architect',
        domain: 'WEB',
        technologies: [],
        bandwidth: '50%'
    },
    {
        name: 'Chandan Jha',
        role: 'Technical Lead',
        domain: 'WEB',
        technologies: ['Java', 'Springboot']
    },
    {
        name: 'Devesh Ballani',
        role: 'Sr. Software Engineer',
        domain: 'WEB',
        technologies: ['JavaScript', 'Angular', '.Net Core']
    },
    {
        name: 'Preetam Kumar',
        role: 'Software Engineer',
        domain: 'WEB',
        technologies: ['Java', 'Springboot', 'Hybris'],
        bandwidth: '100%'
    },
    {
        name: 'Shahrukh Khan',
        role: 'Software Engineer',
        domain: 'WEB',
        technologies: ['Angular', 'Java', '.NET']
    },
    {
        name: 'Talib Hussain',
        role: 'Jr. Software Engineer',
        domain: 'WEB',
        technologies: ['Angular', 'Java', 'Springbot']
    }
];





