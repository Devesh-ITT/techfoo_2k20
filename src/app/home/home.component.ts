import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { InputDataDialogComponent } from '../dialog/input-data-dialog/input-data-dialog.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { ResourcesViewDialogComponent } from '../dialog/resources-view-dialog/resources-view-dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
  }

  askInput(code: string) {
    this.dialog.open(InputDataDialogComponent, {
      width: code === 'A' ? '300px' : (code === 'B' ? '300px' : (code === 'C' ? '300px' : '300px')),
      height: code === 'A' ? '250px' : (code === 'C' ? '300px' : (code === 'B' ? '300px' : '300px')),
      data: { inputCode: code },
    })
    .afterClosed().subscribe((result) => {
      console.log(result);
      if (result) {
        if (result.action === 'findResource') {
          this.spinner.show();
          setTimeout(() => {
            this.spinner.hide();
            this.dialog.open(ResourcesViewDialogComponent, {
              disableClose: true,
              height: '650px',
              width: '1000px',
              data: result
            })
            .afterClosed().subscribe((afterCloseResponse) => {
              console.log(afterCloseResponse);
            });
          }, 2000);
        }
      }
    });
  }

}
