import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pre',
  templateUrl: './pre.component.html',
  styleUrls: ['./pre.component.css']
})
export class PreComponent implements OnInit {
  currentResources = [
    {
      name: 'Spring Boot (Java)',
      resourceCount: 2,
    },
    {
      name: '.Net Core',
      resourceCount: 2,
    },
    {
      name: 'React Native',
      resourceCount: 1,
    },
    {
      name: 'Power BI',
      resourceCount: 2,
    },
  ];

  futureResources = [
    {
      name: 'Spring Boot (Java)',
      resourceCount: 4,
    },
    {
      name: '.Net Core',
      resourceCount: 5,
    },
    {
      name: 'Node JS',
      resourceCount: 6,
    },
    {
      name: 'C/C++',
      resourceCount: 4,
    },
    {
      name: 'React Native',
      resourceCount: 5,
    },
    {
      name: 'Power BI',
      resourceCount: 2,
    },
    {
      name: 'ElectronJS',
      resourceCount: 3,
    },
    {
      name: 'Android',
      resourceCount: 2,
    },
    {
      name: 'Node JS',
      resourceCount: 6,
    },
    {
      name: 'PHP',
      resourceCount: 2,
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
