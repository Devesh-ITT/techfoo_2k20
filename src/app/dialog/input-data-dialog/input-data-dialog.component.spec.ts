import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputDataDialogComponent } from './input-data-dialog.component';

describe('InputDataDialogComponent', () => {
  let component: InputDataDialogComponent;
  let fixture: ComponentFixture<InputDataDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputDataDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDataDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
