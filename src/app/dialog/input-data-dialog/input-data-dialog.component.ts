import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-input-data-dialog',
  templateUrl: './input-data-dialog.component.html',
  styleUrls: ['./input-data-dialog.component.css']
})
export class InputDataDialogComponent implements OnInit {

  code: string;
  domains = [
    {
      name: 'Web',
      code: 'WEB'
    },
     {
       name: 'Mobile',
       code: 'MOBILE'
     },
     {
       name: 'Embedded',
       code: 'EMBEDDED',
     },
     {
       name: 'Artificial Intelligence',
       code: 'AI'
     }
  ];
  languages = [
    {
      name: 'C/C++',
      code: 'CCPP'
    },
    {
      name: 'Java',
      code: 'JAVA'
    },
    {
      name: '.Net/.Net Core',
      code: 'DOTNET_DOTNOTCORE'
    },
    {
      name: 'Python',
      code: 'Python'
    },
    {
      name: 'React',
      code: 'REACT',
    },
    {
      name: 'Angular',
      code: 'Angular',
    },
    {
      name: 'Vue js',
      code: 'VueJS'
    },
    {
      name: 'Android',
      code: 'Android'
    },
    {
      name: 'Swift',
      code: 'Swift',
    },
    {
      name: 'Manual QA',
      code: 'MQA',
    },
    {
      name: 'Automation',
      code: 'AQA',
    }
  ];
  projects = [
    {
      name: 'KDK GST Software',
      code: 'KDKGSTSOFTWARE'
    },
    {
      name: 'Collect.Express',
      code: 'COLLECTEXPRESS'
    },
    {
      name: 'RFIDEAS',
      code: 'RFIDEAS',
    },
    {
      name: 'CII',
      code: 'CII'
    },
    {
      name: 'Pharos',
      code: 'PHAROS'
    },
    {
      name: 'HP Daas Estimator',
      code: 'DaasEstimator'
    },
    {
      name: 'HP Configurator',
      code: 'HPC'
    },
    {
      name: 'HPSC',
      code: 'HPSC'
    }
  ];
  selectedDomain: any;
  techStack: any[];
  duration: any;
  selectedProject: any;
  selectedTech: any;
  totalHeadCount = 1;

  constructor(
    public dialogRef: MatDialogRef<InputDataDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data.inputCode);
    this.code = this.data.inputCode;
  }

  findResource() {
    if (this.code === 'A') {
      this.dialogRef.close({
        action: 'findResource',
        code: 'A',
        data: {
          selectedDomain: this.selectedDomain,
          techStack: this.techStack
        }
      });
    } else if (this.code === 'C') {
      this.dialogRef.close({
        action: 'findResource',
        code: 'C',
        data: {
          selectedProject: this.selectedProject,
          selectedTech: this.selectedTech,
          totalHeadCount: this.totalHeadCount,
        }
      });
    }
  }

}
