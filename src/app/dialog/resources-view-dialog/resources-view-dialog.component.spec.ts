import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcesViewDialogComponent } from './resources-view-dialog.component';

describe('ResourcesViewDialogComponent', () => {
  let component: ResourcesViewDialogComponent;
  let fixture: ComponentFixture<ResourcesViewDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourcesViewDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcesViewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
