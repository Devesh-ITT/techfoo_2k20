import { Component, OnInit, Inject, OnChanges } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DATA_FOR_CODE_A } from '../../data/a';

@Component({
  selector: 'app-resources-view-dialog',
  templateUrl: './resources-view-dialog.component.html',
  styleUrls: ['./resources-view-dialog.component.css']
})
export class ResourcesViewDialogComponent implements OnInit, OnChanges {
  selectedData: any = [];
  disagreed = false;
  code: string;

  constructor(
    public dialogRef: MatDialogRef<ResourcesViewDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnChanges() {

  }

  ngOnInit() {
    console.log(this.data);
    this.code = this.data.code;
    if (this.data && this.data.code === 'A') {
      this.selectedData = DATA_FOR_CODE_A;
      console.log(this.selectedData);
    }
  }

  disagree() {
    this.disagreed = true;
  }

  cancelDisagree() {
    this.disagreed = false;
  }

  close() {
    this.dialogRef.close();
  }
}
